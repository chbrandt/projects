# PUP (former 0-waste people)

The goal of this "project" is to make unproductive people, marginalized people part of the productive work force.
As signaled already, by _unproductive_ I mean to say people that -- for educational reasons; the lack of it -- are marginalized from the economically active society.

Out there, people and organizations, governmental and non-governmental, do everyday a great deal of social work in bringing education and dignity to many marginalized people. That is done through basic education or technical training, to improve either basic human/social needs or professional skills. The ultimately goal is one: guarantee a dignifying life for those people; understanding that being part of a valuable society is a fundamental component for a person personal satisfaction direct those (social workers) efforts to projects about bringing those people to the productive, economically active society at some point in the short-/long-term period.

...

That being said, the idea here is to rethink, rephrase the goals and redraw the route taken to add value to those people and consequently the whole society.

I think we can actually bring interest to the matter and make it -- not only the people, but process itself -- a big part of the economy of a society. To do that we have to define clear goals, capable of being fine-measured; Having clear goals and the ability to evaluate (as fine as possible) we will be able to _optimize_ the process.

Optimize the process means:
* identify people' skills and capabilities (as best as possible)
* shorten the amount of time somebody becomes and feel productive

> Note: there is a general idea underlying all this discussion and that is the fact a resource in a society is _wasted_ when (i) there is too much of it (_i.e_, not enough demand) and (ii) the resource is expensive or laborious to profit from and demand for that particular resource is just not created.
> * The point being made here, then, is that the resource is there and _must_ (because...people!) be workedout to improve the overall society and everybody's quality of life. So, we have to (i) improve de quality of the resource _and_ (ii) create/increase the demand.

